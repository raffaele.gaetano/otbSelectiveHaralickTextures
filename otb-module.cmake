set(DOCUMENTATION "OTB Selective Haralick Texture Indices Extraction.")

otb_module(OTBAppSelectiveHaralickTextures
  DEPENDS
    OTBTextures
    OTBITK
    OTBImageBase
    OTBApplicationEngine
    OTBImageManipulation
    OTBObjectList

  DESCRIPTION
    "${DOCUMENTATION}"
)
