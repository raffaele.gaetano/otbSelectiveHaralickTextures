#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbScalarImageToTexturesFilter.h"
#include "otbScalarImageToAdvancedTexturesFilter.h"

#include "otbMultiToMonoChannelExtractROI.h"
#include "otbClampImageFilter.h"
#include "otbImageList.h"
#include "otbImageListToVectorImageFilter.h"

#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"

#include "otbScalarImageToTexturesFilter.h"
#include "otbScalarImageToAdvancedTexturesFilter.h"
#include "otbScalarImageToHigherOrderTexturesFilter.h"

#include "otbMultiToMonoChannelExtractROI.h"
#include "otbClampImageFilter.h"
#include "otbImageList.h"
#include "otbImageListToVectorImageFilter.h"

namespace otb
{
namespace Wrapper
{

class SelectiveHaralickTextures : public Application
{
public:
/** Standard class typedefs. */
typedef SelectiveHaralickTextures     Self;
typedef Application                   Superclass;
typedef itk::SmartPointer<Self>       Pointer;
typedef itk::SmartPointer<const Self> ConstPointer;

typedef MultiToMonoChannelExtractROI<FloatVectorImageType::InternalPixelType,FloatVectorImageType::InternalPixelType>
                                                                               ExtractorFilterType;
typedef ClampImageFilter<FloatImageType, FloatImageType>                       ClampFilterType;

typedef ScalarImageToTexturesFilter<FloatImageType, FloatImageType>            HarTexturesFilterType;
typedef ScalarImageToAdvancedTexturesFilter<FloatImageType, FloatImageType>    AdvTexturesFilterType;

typedef HarTexturesFilterType::SizeType                                        RadiusType;
typedef HarTexturesFilterType::OffsetType                                      OffsetType;

typedef ImageList<FloatImageType>                                              ImageListType;
typedef ImageListToVectorImageFilter<ImageListType, FloatVectorImageType>      ImageListToVectorImageFilterType;


/** Standard macro */
itkNewMacro(Self);

itkTypeMacro(SelectiveHaralickTextures, otb::Application);

private:

void DoInit() ITK_OVERRIDE
{
SetName("SelectiveHaralickTextures");
SetDescription("Computes selective Haralick textures indices on every pixel of the input image selected channel");

// Documentation
SetDocLongDescription("This application computes Haralick textures on a mono band image allowing the choice of specific indices.");
SetDocLimitations("None");
SetDocAuthors("Raffaele Gaetano");
SetDocSeeAlso("otbScalarImageToTexturesFilter and otbScalarImageToAdvancedTexturesFilter classes");

AddDocTag("Textures");
AddDocTag(Tags::FeatureExtraction);

AddParameter(ParameterType_InputImage, "in",  "Input Image");
SetParameterDescription("in", "The input image to compute the features on.");

AddParameter(ParameterType_Int,  "channel",  "Selected Channel");
SetParameterDescription("channel", "The selected channel index");
SetDefaultParameterInt("channel", 1);
SetMinimumParameterIntValue("channel", 1);

AddRAMParameter();

AddParameter(ParameterType_Group, "parameters", "Texture feature parameters");
SetParameterDescription("parameters","This group of parameters allows one to define texture parameters.");

AddParameter(ParameterType_Int,"parameters.xrad","X Radius");
SetParameterDescription("parameters.xrad", "X Radius");
SetDefaultParameterInt("parameters.xrad", 1);

AddParameter(ParameterType_Int,"parameters.yrad","Y Radius");
SetParameterDescription("parameters.yrad", "Y Radius");
SetDefaultParameterInt("parameters.yrad", 1);

AddParameter(ParameterType_Int,"parameters.xoff","X Offset");
SetParameterDescription("parameters.xoff", "X Offset");
SetDefaultParameterInt("parameters.xoff", 1);

AddParameter(ParameterType_Int,"parameters.yoff","Y Offset");
SetParameterDescription("parameters.yoff", "Y Offset");
SetDefaultParameterInt("parameters.yoff", 1);

AddParameter(ParameterType_Float,"parameters.min","Image Minimum");
SetParameterDescription("parameters.min", "Image Minimum");
SetDefaultParameterFloat("parameters.min", 0);

AddParameter(ParameterType_Float,"parameters.max","Image Maximum");
SetParameterDescription("parameters.max", "Image Maximum");
SetDefaultParameterFloat("parameters.max", 8192);

AddParameter(ParameterType_Int,"parameters.nbbin","Histogram number of bin");
SetParameterDescription("parameters.nbbin", "Histogram number of bin");
SetDefaultParameterInt("parameters.nbbin", 64);

AddParameter(ParameterType_ListView, "indices", "Texture Indices Selection");
SetParameterDescription("indices", "Choice of the texture indices to compute. Current version includes energy, entropy, correlation, contrast, mean, variance and dissimilarity.");
AddChoice("indices.energy","energy");
AddChoice("indices.entropy","entropy");
AddChoice("indices.correlation","correlation");
AddChoice("indices.contrast","contrast");
AddChoice("indices.mean","mean");
AddChoice("indices.variance","variance");
AddChoice("indices.dissimilarity","dissimilarity");

AddParameter(ParameterType_OutputImage, "out", "Output Image");
SetParameterDescription("out", "Output image containing the selected texture features.");
MandatoryOff("out");
EnableParameter("out");

// Doc example parameter settings
SetDocExampleParameterValue("in", "qb_RoadExtract.tif");
SetDocExampleParameterValue("channel", "2");
SetDocExampleParameterValue("parameters.xrad", "3");
SetDocExampleParameterValue("parameters.yrad", "3");
SetDocExampleParameterValue("indices", "entropy mean");
SetDocExampleParameterValue("out", "HaralickEntropy.tif");
}

void DoUpdateParameters() ITK_OVERRIDE
{
  // Nothing to do here : all parameters are independent
}

void DoExecute() ITK_OVERRIDE
{
  FloatVectorImageType::Pointer inImage = GetParameterImage("in");
  inImage->UpdateOutputInformation();
  int nBComp = inImage->GetNumberOfComponentsPerPixel();

  if( GetParameterInt("channel") > nBComp )
    {
    itkExceptionMacro(<< "The specified channel index is invalid.");
    }

  RadiusType radius;
  radius[0] = GetParameterInt("parameters.xrad");
  radius[1] = GetParameterInt("parameters.yrad");

  OffsetType offset;
  offset[0] = GetParameterInt("parameters.xoff");
  offset[1] = GetParameterInt("parameters.yoff");

  m_ExtractorFilter = ExtractorFilterType::New();
  m_ExtractorFilter->SetInput(inImage);
  m_ExtractorFilter->SetStartX(inImage->GetLargestPossibleRegion().GetIndex(0));
  m_ExtractorFilter->SetStartY(inImage->GetLargestPossibleRegion().GetIndex(1));
  m_ExtractorFilter->SetSizeX(inImage->GetLargestPossibleRegion().GetSize(0));
  m_ExtractorFilter->SetSizeY(inImage->GetLargestPossibleRegion().GetSize(1));
  m_ExtractorFilter->SetChannel(GetParameterInt("channel"));
  m_ExtractorFilter->UpdateOutputInformation();

  m_ClampFilter = ClampFilterType::New();
  m_ClampFilter->SetInput(m_ExtractorFilter->GetOutput());
  m_ClampFilter->SetThresholds(GetParameterFloat("parameters.min"), GetParameterFloat("parameters.max"));

  m_HarTexFilter    = HarTexturesFilterType::New();
  m_AdvTexFilter    = AdvTexturesFilterType::New();
  m_HarImageList    = ImageListType::New();
  m_HarConcatener   = ImageListToVectorImageFilterType::New();

  m_HarTexFilter->SetInput(const_cast<FloatImageType*>(m_ClampFilter->GetOutput()));
  m_HarTexFilter->SetRadius(radius);
  m_HarTexFilter->SetOffset(offset);
  m_HarTexFilter->SetInputImageMinimum(GetParameterFloat("parameters.min"));
  m_HarTexFilter->SetInputImageMaximum(GetParameterFloat("parameters.max"));
  m_HarTexFilter->SetNumberOfBinsPerAxis(GetParameterInt("parameters.nbbin"));
  m_HarTexFilter->UpdateOutputInformation();
  
  m_AdvTexFilter->SetInput(const_cast<FloatImageType*>(m_ClampFilter->GetOutput()));
  m_AdvTexFilter->SetRadius(radius);
  m_AdvTexFilter->SetOffset(offset);
  m_AdvTexFilter->SetInputImageMinimum(GetParameterFloat("parameters.min"));
  m_AdvTexFilter->SetInputImageMaximum(GetParameterFloat("parameters.max"));
  m_AdvTexFilter->SetNumberOfBinsPerAxis(GetParameterInt("parameters.nbbin"));
  
  const int nbFeatures = GetSelectedItems("indices").size();
  
  for ( int idx=0; idx<nbFeatures; idx++ ) {
	  
	  if ( GetSelectedItems("indices")[idx] == 0 )
		  m_HarImageList->PushBack(m_HarTexFilter->GetEnergyOutput());
	  else if ( GetSelectedItems("indices")[idx] == 1 )
		  m_HarImageList->PushBack(m_HarTexFilter->GetEntropyOutput());
	  else if ( GetSelectedItems("indices")[idx] == 2 )
		  m_HarImageList->PushBack(m_HarTexFilter->GetCorrelationOutput());
	  else if ( GetSelectedItems("indices")[idx] == 3 )
		  m_HarImageList->PushBack(m_HarTexFilter->GetInertiaOutput());
	  else if ( GetSelectedItems("indices")[idx] == 4 )
		  m_HarImageList->PushBack(m_AdvTexFilter->GetMeanOutput());
	  else if ( GetSelectedItems("indices")[idx] == 5 )
		  m_HarImageList->PushBack(m_AdvTexFilter->GetVarianceOutput());
	  else if ( GetSelectedItems("indices")[idx] == 6 )
		  m_HarImageList->PushBack(m_AdvTexFilter->GetDissimilarityOutput());
	  
  }
  
  m_HarConcatener->SetInput(m_HarImageList);
  SetParameterOutputImage("out", m_HarConcatener->GetOutput());
  
}
ExtractorFilterType::Pointer m_ExtractorFilter;
ClampFilterType::Pointer     m_ClampFilter;

HarTexturesFilterType::Pointer            m_HarTexFilter;
ImageListType::Pointer                    m_HarImageList;
ImageListToVectorImageFilterType::Pointer m_HarConcatener;
AdvTexturesFilterType::Pointer            m_AdvTexFilter;
};
}
}

OTB_APPLICATION_EXPORT(otb::Wrapper::SelectiveHaralickTextures)
